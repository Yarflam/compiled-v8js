# Compiled v8js for Ubuntu 18.x / 19.x.

## Prerequisites

You should have the PHP version 7.2 and Git on your Ubuntu 18.x / 19.x.

```bash
apt-get update && apt-get install -y --no-install-recommends php7.2-cli git
```

## Install

1 - Clone the repository in a temporary folder

```bash
cd /tmp && git clone https://gitlab.com/Yarflam/compiled-v8js
```

2 - Execute the script

```bash
cd /tmp/compiled-v8js && chmod +x install.sh && ./install.sh
```

3 - Check the installation

```bash
php -r "var_dump((new v8Js())->executeString('new Date().toISOString();'));"
```

Enjoy your day ! :)

## Source

Extract from a Docker container with [v8js/v8js](https://hub.docker.com/r/v8js/v8js).
